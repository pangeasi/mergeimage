const jimp = require('jimp');
const express = require('express');
const app = express();
const path = require('path')
const fs = require('fs');
const text2png = require('text2png');

app.use(express.static('babylon') );
app.use(express.static('images') );


app.get('/:shirt',(require,response)=>{
  if(require.params.shirt === 'camiseta1'){
    db = [
      {
        src: 'images/body.png',
        posX: 400,
        posY: 125
      },
      {
        src: 'images/eyes.png',
        posX: 400,
        posY: 125
        
      },
      {
        src: 'images/mouth.png',
        posX: 400,
        posY: 125
      }
    ]
  }
  if(require.params.shirt === 'camiseta2'){
    db = [

      {
        src: 'images/body.png',
        posX: 125,
        posY: 200
      },
      {
        src: 'images/eyes.png',
        posX: 125,
        posY: 200
        
      },
      {
        src: 'images/mouth.png',
        posX: 125,
        posY: 200
      },
      {
        src: '',
        posX: 110,
        posY: 400
      }
    ]
  }
  if(require.params.shirt === 'camiseta3'){
    db = [
      {
        src: '',
        posX: 125,
        posY: 200
      },
      {
        src: 'images/body.png',
        posX: 125,
        posY: 200
      },
      {
        src: 'images/eyes.png',
        posX: 125,
        posY: 200
        
      },
      {
        src: 'images/mouth.png',
        posX: 125,
        posY: 200
      }
    ]
  }
  (async()=>{
    await fs.writeFileSync(`images/${require.params.shirt}/font.png`, text2png('goToShirt!', {  
      font: '100px raustila',
      color: 'black',
      localFontPath: 'images/raustila-Regular.ttf',
      localFontName: 'raustila-Regular',
      backgroundColor: 'transparent',
      padding: 20
    }));
    
  })();
  
  
  const base = ['images/base.png'];
  const font = [`images/${require.params.shirt}/font.png`];
  const images = [...base,...db.filter(x => x.src !== '').map(y => y.src),...font]
  console.log(images);
  const jimps = images.map(x => jimp.read(x));
  
  
  Promise.all(jimps)
  .then(async data => {
    await data.map((x,i) => i !== 0 ? data[0].composite(x,db[i-1].posX,db[i-1].posY) : null  )
    //await jimp.loadFont(jimp.FONT_SANS_128_BLACK).then(font => data[0].print(font,120,150,'faker'));
   
    data[0].write(`images/${require.params.shirt}/UVMS1_colorgrid.png`, () => console.log("wrote the image"));
    response.send(`
    <!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8"/>
    <title>Babylon - Getting Started</title>
    <!-- Link to the last version of BabylonJS -->
    <script src="https://preview.babylonjs.com/babylon.js"></script>
    <!-- Link to the last version of BabylonJS loaders to enable loading filetypes such as .gltf -->
    <script src="https://preview.babylonjs.com/loaders/babylonjs.loaders.min.js"></script>
    <!-- Link to pep.js to ensure pointer events work consistently in all browsers -->
    <script src="https://code.jquery.com/pep/0.4.1/pep.js"></script>
    <style>
        html, body {
            overflow: hidden;
            width   : 100%;
            height  : 100%;
            margin  : 0;
            padding : 0;
        }

        #renderCanvas {
            width   : 100%;
            height  : 100%;
            touch-action: none;
        }
    </style>
</head>
<body>
    <canvas id="renderCanvas"></canvas>
    <script>

    window.addEventListener('DOMContentLoaded', function() {
      // All the following code is entered here.
      var canvas = document.getElementById('renderCanvas');
      var engine = new BABYLON.Engine(canvas, true);

      var createScene = function () {
          var scene = new BABYLON.Scene(engine);
          scene.ambientColor = new BABYLON.Color3(0.1, 0.1, 0.1);
          //Adding a light
          var light = new BABYLON.HemisphericLight("Hemi", new BABYLON.Vector3(0, 1, 0), scene);

          //Adding an Arc Rotate Camera
          var camera = new BABYLON.ArcRotateCamera("Camera", -1.85, 1.2, 1.5, BABYLON.Vector3.Zero(), scene);
          camera.attachControl(canvas, true);

          
	        var design = new BABYLON.StandardMaterial("design", scene);
design.diffuseTexture = new BABYLON.Texture("test.png", scene);
          // The first parameter can be used to specify which mesh to import. Here we import all meshes
          var shirt = BABYLON.SceneLoader.ImportMesh("", "./", "shirt.babylon", scene, function (newMeshes) {
              newMeshes.map(x => {
                x.material = design;
                x.sideOrientation = BABYLON.Mesh.DOUBLESIDE;
              
              })

              
            });
            
        
          return scene;
      };
      var scene = createScene();
      engine.runRenderLoop(function() {
          scene.render();
      });
      window.addEventListener('resize', function() {
          engine.resize();
      });
  });
  </script>
  </body>
  </html>
  
  
    
    `);
  });

})

app.listen(3333,()=>console.log('listen on port 3333'));
